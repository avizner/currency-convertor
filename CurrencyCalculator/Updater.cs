﻿using System;
using System.IO;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;


namespace CurrencyCalculator
{
    internal class Updater
    {
        private static readonly Uri ecbUrl = new Uri("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
        static string _ratesXmlPath = Path.Combine(Environment.CurrentDirectory, @"..\..\Resources\eurofxref-daily.xml");
        static XDocument _ecbResponce = new XDocument();
        static readonly NetworkManager NetManager = new NetworkManager();
        private static bool _isUpdated;
        static Timer timer = new Timer();

        internal static void Initializer(int timeout)
        {
            timer.Tick += new EventHandler(Worker); // Everytime timer ticks, timer_Tick will be called
            timer.Interval = 60000 * timeout;              // Timer will tick evert second
            timer.Enabled = true;                       // Enable the timer
            timer.Start();                              // Start the timer
        }

        static void Worker(object sender, EventArgs e)
        {
            //MessageBox.Show("Worker");
           
            

            // If file exists and valide so getting last time update
            if (File.Exists(_ratesXmlPath) && IsValidXml(_ratesXmlPath))
            {
                _ecbResponce = XDocument.Load(_ratesXmlPath);
                _isUpdated = IsUpdated(_ecbResponce);
            }

            // Else create one new and try to download from internet
            File.Create(_ratesXmlPath);

            if (NetManager.IsConnected())
            {
                while (!_isUpdated)
                {
                    _ecbResponce = NetManager.DoSingleRequest(ecbUrl);

                    // Parse the response and store rates in dictionary
                    Parser.GetRatesList(_ecbResponce);

                    // NB! Here I have been used stub because of some troubles with multithread sharing of file
                    // Thank you for your understanding 
                    //netManager.SaveRequestToFile(ecbResponce, _ratesXmlPath);

                    //MessageBox.Show(string.Format("Unable to fetching rates from internet."));

                    if (IsValidXml(_ratesXmlPath) && _isUpdated)
                    {
                        break;
                    }
                }
            }
            else
            {
                MessageBox.Show("Unable to connect to remote server.\nNo internet connection is available.");
            }
        }

        // Detect if xml is malformed
        public static bool IsValidXml(string path)
        {
            try
            {
               XDocument.Load(path);
                return true;
            }
            catch (XmlException e)
            {
                return false;
            }
        }

        public static bool IsUpdated(XDocument xdoc)
        {
            DateTime lastUpdateTime = Parser.GetTimeOfUpdate(xdoc);
            DateTime today = DateTime.Today;

            // Return true if date of xml is meet to today or today is weekend because 
            // ECB doesn't perform update on weekend 
            return lastUpdateTime.Equals(today) ||
                   today.DayOfWeek == DayOfWeek.Saturday ||
                   today.DayOfWeek == DayOfWeek.Sunday;
        }
    }
}
