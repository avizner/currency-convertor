﻿//////////////////////////////////////////////////////////////////////////////
///  This application supplies thesis work "The practical usage of unit    ///
/// testing on example of development web-client"                          ///
///                                                                        ///
/// Student: Alexander Vizner                                              ///
/// Place: Tallinn                                                         ///
/// Year: 2012                                                             ///
//////////////////////////////////////////////////////////////////////////////

using System.Drawing;
using System.Windows.Forms;

namespace CurrencyCalculator
{
    public class CustomButton : Button
    {

        #region Fields and properties

        public int Transparency { get; set; }
        public Image Icon { get; set; }
        public Point Place { get; set; }
        public Rectangle Area { get; set; }
        
        #endregion

        public CustomButton() { }

        public CustomButton(Bitmap icon, int transparency, Point location)
        {
            Icon = icon;
            Transparency = transparency;
            Place = location;
            Area = new Rectangle(Place, Icon.Size);
        }

        // Drowing custom button 
        public void RenderControl(Graphics g)
        {
            g.DrawImage(Icon, Place.X, Place.Y, 20, 20);
        }
    }
}
