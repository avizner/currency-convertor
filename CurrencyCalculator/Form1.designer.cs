﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace CurrencyCalculator
{
    sealed partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private Bitmap _formBackGround;
        private Bitmap _settingsButtonIcon;
        private Bitmap _divider;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            #region Custom controls

            try
            {
                _settingsButtonIcon = new Bitmap(
                    Path.Combine(Environment.CurrentDirectory, @"..\..\Resources\Icons\Settings.png")
                    );
                _formBackGround = new Bitmap(
                    Path.Combine(Environment.CurrentDirectory, @"..\..\Resources\Icons\BG.png")
                    );
                _divider = new Bitmap(
                   Path.Combine(Environment.CurrentDirectory, @"..\..\Resources\Icons\Divider.png")
                   );
            }

            catch (Exception e)
            {
                throw new FileNotFoundException();
            }
            settingsButton = new CustomButton(_settingsButtonIcon, 1, new Point(Width - 53, 94));
            
            BackgroundImage = _formBackGround;
            BackgroundImageLayout = ImageLayout.Tile;
            //Opacity = 0.9;
            FormBorderStyle = FormBorderStyle.None;

            cTextBox1 = new CustomTextBox();
            cTextBox2 = new CustomTextBox();

            #endregion

            // Standart controls init
            this.combo1 = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.combo2 = new System.Windows.Forms.ComboBox();
            this.label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            //
            // Custom text box 1
            //
            cTextBox1.Bounds = new Rectangle(156, 35, 77, 20);
            this.cTextBox1.Name = "textBox1";
            this.cTextBox1.TextAlign = HorizontalAlignment.Right;
            this.cTextBox1.KeyUp += new KeyEventHandler(TextBoxTextChanged);
            //
            // Custom text box 2
            //
            cTextBox2.Bounds = new Rectangle(156, 61, 77, 20);
            this.cTextBox2.Name = "textBox2";
            this.cTextBox2.TextAlign = HorizontalAlignment.Right;
            this.cTextBox2.KeyUp += new KeyEventHandler(TextBoxTextChanged);
            // 
            // combo1
            // 
            this.combo1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo1.DropDownWidth = 120;
            this.combo1.IntegralHeight = false;
            this.combo1.Location = new System.Drawing.Point(12, 34);
            this.combo1.MaxDropDownItems = 7;
            this.combo1.Name = "combo1";
            this.combo1.Size = new System.Drawing.Size(138, 21);
            this.combo1.TabIndex = 1;
            this.combo1.Sorted = true;
            this.combo1.SelectionChangeCommitted += new EventHandler(ComboSelectedIndexChanged);
            // 
            // combo2
            // 
            this.combo2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo2.DropDownWidth = 120;
            this.combo2.IntegralHeight = false;
            this.combo2.Location = new System.Drawing.Point(12, 61);
            this.combo2.MaxDropDownItems = 7;
            this.combo2.Name = "combo2";
            this.combo2.Text = "22";
            this.combo2.Size = new System.Drawing.Size(138, 21);
            this.combo2.TabIndex = 2;
            this.combo2.Sorted = true;
            this.combo2.SelectionChangeCommitted += new EventHandler(ComboSelectedIndexChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Location = new System.Drawing.Point(240, 34);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 20);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Location = new System.Drawing.Point(240, 61);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(32, 21);
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.SizeMode = PictureBoxSizeMode.CenterImage;
            //
            // Label
            //
            this.label.Size = new Size(221, 22);
            this.label.Location = new Point(12, 88);
            this.label.BackColor = Color.Transparent;
            this.label.Name = "label";
            this.label.Font = new Font("Tahoma", 9.0F);
            this.label.ForeColor = Color.WhiteSmoke;
            this.label.TextAlign = ContentAlignment.BottomCenter;
            // 
            // Form1
            // 
            this.BackColor = Color.FromArgb(80, 101, 122);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 130);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cTextBox2);
            this.Controls.Add(this.cTextBox1);
            this.Controls.Add(this.combo1);
            this.Controls.Add(this.combo2);
            this.Controls.Add(this.label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Currency convertor";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void HideButtonClick(object sender, EventArgs e)
        {
            MessageBox.Show("test");
        }

        #endregion

        private ComboBox combo2;
        //private CustomButton closeButton;
        //private CustomButton hideButton;
        private CustomButton settingsButton;
        private CustomTextBox cTextBox1;
        private CustomTextBox cTextBox2;
        private ComboBox combo1;
        private PictureBox pictureBox1;
        private PictureBox pictureBox2;
        private Label label;


    }
}

