﻿//////////////////////////////////////////////////////////////////////////////
///  This application supplies thesis work "The practical usage of unit    ///
/// testing on example of development web-client"                          ///
///                                                                        ///
/// Student: Alexander Vizner                                              ///
/// Place: Tallinn                                                         ///
/// Year: 2012                                                             ///
//////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Linq;

namespace CurrencyCalculator
{
    /// <summary>
    /// Gets response from web-service and save it
    /// </summary>
    public class NetworkManager
    {
        // Test site
        private const string Google = "www.google.com";
        public string Provider { get; set; }

        # region Constructors

        // Default constructor
        public NetworkManager()
        {
            Provider = Google;
        }

        // Constructor with given provider
        public NetworkManager(string provider)
        {
            Provider = provider;
        }

        # endregion

        #region Methods

        /// <summary>
        /// Verify if network connection is available 
        /// </summary>
        /// <returns>Boolean</returns>
        public bool IsConnected()
        {
            // Check if connection is established
            if (NetworkInterface.GetIsNetworkAvailable() && Provider != string.Empty)
            {
                try
                {
                    // Try to resolve www.google.com by hostname and get IP adress
                    IPAddress[] adr = Dns.GetHostAddresses(Provider);
                    if (adr.Length > 0)
                    {
                        return true;
                    }
                }
                catch(ArgumentException e)
                {
                    MessageBox.Show(string.Format("{0}: Unable to find such host: {1}", DateTime.Now, e.Message));
                }
            }
            return false;
        }

        /// <summary>
        /// Checking connection to network
        /// </summary>
        private void CheckConnectivity()
        {
            // Check if connection is available
            if (!IsConnected())
            {
                MessageBox.Show(string.Format("{0}: Connection to network is unavailable.", DateTime.Now));
            }
        }

        /// <summary>
        /// Initiate single request by givet URL
        /// </summary>
        /// <param name="url">Full URI to remote service</param>
        /// <returns>String</returns>
        public XDocument DoSingleRequest(string url)
        {
            CheckConnectivity();

            string result = "";


            // Create the web request  
            if (!string.IsNullOrEmpty(url))
            {
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                if (request != null)
                {
                    request.Timeout = 500;

                    // Get response  
                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        // Get the response stream  
                        if (response != null)
                        {
                            StreamReader reader = new StreamReader(response.GetResponseStream());

                            // Read contents and return as a string  
                            result = reader.ReadToEnd();
                        }
                    }
                }
            }
            else
            {
                throw new ArgumentNullException();
            }

            return XDocument.Parse(result);
        }

        /// <summary>
        /// Do single request using XDocument class
        /// </summary>
        /// <param name="url">Strong named uri to remote xml file</param>
        /// <returns>XDocument</returns>
        public XDocument DoSingleRequest(Uri url)
        {
            CheckConnectivity();

            XDocument doc = new XDocument();
            if (url != null && url.IsWellFormedOriginalString())
            {
                doc = XDocument.Load(url.ToString());
            }
            return doc;
        }

        /// <summary>
        /// Save file to current directory
        /// </summary>
        /// <param name="rates"></param>
        /// <param name="pathToSave">Path to save file</param>
        public void SaveRequestToFile(XDocument rates, string pathToSave)
        {
            // If xml is not null try to save it by given path
            if (rates == null)
            {
                MessageBox.Show(string.Format("{0} Unable to save empty document.", DateTime.Now));
            }
            else
            {
                try
                {
                    rates.Save(pathToSave, SaveOptions.OmitDuplicateNamespaces);
                }
                catch (IOException e)
                {
                    throw new IOException(string.Format("{0} Unable to save file: {1}", DateTime.Now, e.Message));
                }
            }
        }

        /// <summary>
        /// Downloads image by RUL
        /// </summary>
        /// <param name="url">Full URL</param>
        /// <returns>Image</returns>
        public Image DownloadImageByUrl(string url)
        {
            Image ret = null;

            try
            {
                // Initiate a connection
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.AllowWriteStreamBuffering = true;

                // Set request timeout for 20 seconds
                request.Timeout = 20000;

                // Request response:
                WebResponse response = request.GetResponse();

                // Open data stream:
                Stream webStream = response.GetResponseStream();

                // Stream to image
                ret = Image.FromStream(webStream);
                response.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception caught in process: {0}", e.Message);
                ret = null;
            }

            return ret;
        }

        /// <summary>
        /// Download icons picture and returns dictionary data
        /// </summary>
        /// <returns>dictionary of string and bitmaps</returns>
        public Dictionary<string, Bitmap> DownloadResourcesAndGetList()
        {
            string flagsDir = Path.Combine(Environment.CurrentDirectory, @"..\..\Resources\Flags\");
            string path = Path.Combine(Environment.CurrentDirectory, @"eurofxref-daily.xml");
            Dictionary<string, Bitmap> iconsList = new Dictionary<string, Bitmap>();
            NetworkManager netManager = new NetworkManager();

            // Parse the response and store rates in dictionary
            Dictionary<string, double> currencyRateList = Parser.GetRatesList(path);

            // Try to download flags pictures from web
            if (currencyRateList.Count > 0)
            {
                string url = "http://www.ecb.europa.eu/i/flags";
                foreach (KeyValuePair<string, double> rate in currencyRateList)
                {
                    // Receiving image by url
                    Image tmp = netManager.DownloadImageByUrl(string.Format("{0}/{1}.gif", url, rate.Key));

                    // Try to save it locally
                    try
                    {
                        using (Bitmap bm = new Bitmap(tmp))
                        {
                            // Populating icons list
                            iconsList.Add(rate.Key, bm);

                            // ...and save then locally on disk
                            bm.Save(Path.Combine(flagsDir, rate.Key + ".png"), System.Drawing.Imaging.ImageFormat.Png);
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("An Error has occured: \n" + e.Message);
                    }
                }
            }

           return iconsList;
        }

        /// <summary>
        /// Convert currency via requesting google financial service
        /// </summary>
        /// <param name="amount">Amount of currency</param>
        /// <param name="fromCurrency">Source currency</param>
        /// <param name="toCurrency">Target currency</param>
        /// <returns></returns>
        public static double RequestGoogle(double amount, string fromCurrency, string toCurrency)
        {
            // If arguments are correct and not empty request google finance service for convertation
            if(string.IsNullOrEmpty(fromCurrency) || string.IsNullOrEmpty(toCurrency))
            {
                MessageBox.Show("Incorrect value.");
            }

            WebClient web = new WebClient();
            
            string url = string.Format("http://www.google.com/ig/calculator?hl=en&q={0}{1}%3D%3F{2}", 
                amount, 
                fromCurrency.ToUpper(), 
                toCurrency.ToUpper());
 
            string response = web.DownloadString(url);
            string ret1 = response.Split('"')[3].Split(' ')[0].Trim();

            // Removing "magic" invisible sign in returner result
            string ret2 = Regex.Replace(ret1, @"[^\x20-\x7F]", "");
            
            double rate = Convert.ToDouble(ret2);
 
            return rate;
        }

        #endregion
    }
}
