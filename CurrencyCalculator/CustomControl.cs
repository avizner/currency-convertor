﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace CurrencyCalculator
{
    public class CustomControl
    {
        public int Transparency { get; set; }
        public Bitmap Icon { get; set; }
        public Point Place { get; set; }
        public event EventHandler Click;
        public CustomControl() { }

        public CustomControl(Bitmap icon, int transparency, Point location)
        {
            this.Icon = icon;
            this.Transparency = transparency;
            this.Place = location;
            //this.Click += new EventHandler(CustomControlClick);
        }

        public void RenderControl(Graphics g)
        {
            g.DrawImage(Icon, Place);
        }

        public void CustomControlClick(object sender, EventArgs e)
        {
            MessageBox.Show("Hey, you just press me!");
        }
    }
}
