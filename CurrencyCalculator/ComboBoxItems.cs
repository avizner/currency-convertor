﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CurrencyCalculator
{
    public class ComboBoxItems : ComboBox
    {
        private List<ComboBoxItem> _comboBoxItems;

        public List<ComboBoxItem> ComboBoxItemsList
        {
            get { return this._comboBoxItems; }
            set
            {
                this._comboBoxItems = value;
                this.BeginUpdate();
                this.Items.Clear();

                if (value != null)
                    this.Items.AddRange(value.ToArray());

                this.EndUpdate();
            }
        }

        public ComboBoxItems()
        {
            this.DrawMode = DrawMode.OwnerDrawVariable;
            this.DropDownHeight = 34;
            this.DropDownWidth = 200;
            this.DropDownStyle = ComboBoxStyle.DropDownList;
            this.MaxDropDownItems = 4;

            this.DrawItem += new DrawItemEventHandler(ComboBoxItemsDrawItem);
            this.MeasureItem += new MeasureItemEventHandler(ComboBoxItemsMeasureItem);
        }

        private void ComboBoxItemsDrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();

            // Draw item inside comboBox
            if ((e.State & DrawItemState.ComboBoxEdit) != DrawItemState.ComboBoxEdit && e.Index > -1)
            {
                ComboBoxItem item = this.Items[e.Index] as ComboBoxItem;

                e.Graphics.FillRectangle(Brushes.Gray, new Rectangle(e.Bounds.Left + 6, e.Bounds.Top + 6, 22, 22));

                if (item != null)
                {
                    e.Graphics.DrawImage(item.Image, new Rectangle(e.Bounds.Left + 7, e.Bounds.Top + 7, 20, 20));

                    e.Graphics.DrawString(item.FullName, e.Font,
                                          new SolidBrush(e.ForeColor), e.Bounds.Left + 34, e.Bounds.Top + 10);
                }
            }
            // Draw visible text
            else if (e.Index > -1)
            {
                ComboBoxItem item = this.Items[e.Index] as ComboBoxItem;

                e.Graphics.DrawString(item.FullName, e.Font,
                        new SolidBrush(e.ForeColor), e.Bounds.Left, e.Bounds.Top);
            }

            e.DrawFocusRectangle();
        }

        private static void ComboBoxItemsMeasureItem(object sender, MeasureItemEventArgs e)
        {
            e.ItemHeight = 34;
        }
    }
}
