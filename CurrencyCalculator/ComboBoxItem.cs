﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace CurrencyCalculator
{
    public class ComboBoxItem : Control
    {
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public Bitmap Image { get; set; }

       public ComboBoxItem(string fullName, string shortName, Bitmap image)
        {
            FullName = fullName;
            ShortName = shortName;
            Image = image;
        }
    }
}
