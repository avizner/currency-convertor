﻿//////////////////////////////////////////////////////////////////////////////
///  This application supplies thesis work "The practical usage of unit    ///
/// testing on example of development web-client"                          ///
///                                                                        ///
/// Student: Alexander Vizner                                              ///
/// Place: Tallinn                                                         ///
/// Year: 2012                                                             ///
//////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml.Linq;
using NUnit.Framework;

namespace CurrencyCalculator.Tests
{   // Unit test for Parser class
    [TestFixture]
    class ParserTests
    {

        #region Tests

        [Test]      // Check if input has expected values
        public void GetRatesList()
        {
            // Prepare test values
            Dictionary<string, double> initData = new Dictionary<string, double>();
            Dictionary<string, double> retData = new Dictionary<string, double>();

            initData.Add("RUB", 40.9);
            // Incorrect currency name
            initData.Add("aaaaaaaaaaaaaaa", 2.499999999999);

            // Iterate each pairs and create stub of xml
            foreach (KeyValuePair<string, double> pair in initData)
            {
                retData = Parser.GetRatesList(GetXDoc(pair.Key, pair.Value, DateTime.Now));

                // Check if nay data is returned
                Assert.AreNotEqual(retData.Count, null);

                // Verify if input are the same as output of method
                foreach (KeyValuePair<string, double> keyValuePair in retData)
                {
                    Assert.AreEqual(pair.Key, pair.Key);
                    Assert.AreEqual(pair.Value, keyValuePair.Value);
                }
            }
        }

        [Test]
        public void IncorrectXml()
        {
            // Malformed xml document
            XDocument xmlret = XDocument.Load(new StringReader(string.Format("<?xml version=\"1.0\" encoding=\"windows-1251\"?>" +
                                             "<gesmes:Envelope xmlns:gesmes=\"http://www.gesmes.org/xml/2002-08-01\" xmlns=\"http://www.ecb.int/vocabulary/2002-08-01/eurofxref\">" +
                                                "<gesmes:subject>Reference rates</gesmes:subject>" +
                                             "<Cube>" +
                                             "<Cube time=\"04-06-2012\">" +
                                             "<Cube currency=\"EEK\" rate=\"1\" />" +
                                             "</Cube>" +
                                             "</Cube>" +
                                             "<gesmes:Sender>" +
                                             "<gesmes:name>European Central Bank</gesmes:name>" +
                                             "</gesmes:Sender>" +
                                             "</gesmes:Envelope >")));

            // Verifying if method returns not a null
            Assert.IsNotNull(Parser.GetRatesList(xmlret));
        }


        #endregion

        #region Helper methods

        // Helper method for generating xml stub
        static XDocument GetXDoc(string currency, double rate, DateTime date)
        {
            XDocument xmlret = XDocument.Parse(string.Format("<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                                             "<gesmes:Envelope xmlns:gesmes=\"http://www.gesmes.org/xml/2002-08-01\" xmlns=\"http://www.ecb.int/vocabulary/2002-08-01/eurofxref\">" +
                                             "<gesmes:subject>Reference rates</gesmes:subject>" +
                                             "<gesmes:Sender>" +
                                             "<gesmes:name>European Central Bank</gesmes:name>" +
                                             "</gesmes:Sender>" +
                                             "<Cube>" +
                                             "<Cube time=\"{0}\">" +
                                             "<Cube currency=\"{1}\" rate=\"{2}\" />" +
                                             "</Cube>" +
                                             "</Cube>" +
                                             "</gesmes:Envelope >", date.ToString("dd-mm-yyyy", CultureInfo.InvariantCulture), currency, rate));

            return xmlret;
        }

        #endregion
    }
}

