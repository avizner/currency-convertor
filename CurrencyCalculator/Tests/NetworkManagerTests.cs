﻿//////////////////////////////////////////////////////////////////////////////
///  This application supplies thesis work "The practical usage of unit    ///
/// testing on example of development web-client"                          ///
///                                                                        ///
/// Student: Alexander Vizner                                              ///
/// Place: Tallinn                                                         ///
/// Year: 2012                                                             ///
//////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Net.Sockets;
using System.Xml.Linq;
using NUnit.Framework;

namespace CurrencyCalculator.Tests
{
    [TestFixture]
    class NetworkManagerTests
    {
        [Test]      // Verify if empty string was passed to constructor
        public void IsConnected()
        {
            string provider = "";
            NetworkManager network = new NetworkManager(provider);
            Assert.IsFalse(network.IsConnected());
        }

        [Test]      //Verify if default provider is google
        public void IsDefaultProviderCorrect()
        {
            string expected = "www.google.com";
            NetworkManager network = new NetworkManager();
            Assert.AreEqual(expected, network.Provider);
        }

        [Test]      // Verify if nondefault provider was passed to constructor
        public void IncorrectProvierName()
        {
            string providerName = "www.neti.ee";
            NetworkManager network = new NetworkManager(providerName);
            Assert.IsTrue(network.IsConnected());
        }

        [Test]      // Verify that method rises SocketException (No sich host found)
        public void IncorrectProviderNameString()
        {
            string providerName = "eweqrdsfdgfds";
            NetworkManager network = new NetworkManager(providerName);
            Assert.Throws<SocketException>(() => network.IsConnected());
        }

        [Test]      // Check if request string is empty
        public void DoEmptyRequest()
        {
            string url = "";
            NetworkManager network = new NetworkManager();
            Assert.Throws<ArgumentNullException>(() => network.DoSingleRequest(url));
        }

        [Test]      // Check if incorrect path was passed
        public void IncorrectPath()
        {
            string path = @"Y:\test\";
            XDocument xdoc = new XDocument();
            NetworkManager network = new NetworkManager();
            Assert.Throws<IOException>(() => network.SaveRequestToFile(xdoc, path));
        }
    }
}
