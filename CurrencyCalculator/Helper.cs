﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace CurrencyCalculator
{
    class Helper
    {
        /// <summary>
        /// Checks if icons files are available on disk and loads their into ram
        /// </summary>
        /// <param name="path">Path to directory with icons</param>
        /// <returns> List of combobox items</returns>
        static public List<ComboBoxItem> LoadIconsFromDisk(string path)
        {
            List<ComboBoxItem> comboList = new List<ComboBoxItem>();
            string[] iconArray = new string[] { };

            if (!string.IsNullOrEmpty(path) && Directory.Exists(path))
            {
                try
                {
                    iconArray = Directory.GetFiles(path);
                }
                catch (Exception e)
                {
                    throw new FileNotFoundException();
                }

                foreach (string icon in iconArray)
                {
                    using (Bitmap bm = new Bitmap(Path.Combine(path, icon)))
                    {
                        comboList.Add(new ComboBoxItem(Path.GetFileNameWithoutExtension(icon), icon, bm));
                    }
                }
            }

            return comboList;
        }
    }
}
