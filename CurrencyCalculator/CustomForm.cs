﻿//////////////////////////////////////////////////////////////////////////////
///  This application supplies thesis work "The practical usage of unit    ///
/// testing on example of development web-client"                          ///
///                                                                        ///
/// Student: Alexander Vizner                                              ///
/// Place: Tallinn                                                         ///
/// Year: 2012                                                             ///
//////////////////////////////////////////////////////////////////////////////

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CurrencyCalculator
{
    /// <summary>
    /// Describes custom UI element 
    /// </summary>
    public class CustomForm : Form
    {

        #region Fields

        private bool _drag;
        private Point _startPoint = new Point(0, 0);
        private readonly Bitmap _closeButtonIcon;
        private readonly Bitmap _hideButtonIcon;
        private static CustomButton _closeButton;
        public static CustomButton HideButton;
        
        #endregion

        #region Constructors

        public CustomForm()
        {
            // Set common style for the form
            SetStyle(ControlStyles.DoubleBuffer |
            ControlStyles.UserPaint |
            ControlStyles.AllPaintingInWmPaint,
            true);
            UpdateStyles();

            // Try to load bitmaps, if unsuccess so rise exception
            try
            {
                _closeButtonIcon = new Bitmap(
                       Path.Combine(Environment.CurrentDirectory, @"..\..\Resources\Icons\Close.png")
                       );
                _hideButtonIcon = new Bitmap(
                    Path.Combine(Environment.CurrentDirectory, @"..\..\Resources\Icons\Hide.png")
                    );
            }
            catch (FileNotFoundException e)
            {
                MessageBox.Show("During loading application error occures: {0}", e.Message);
            }

            // Initialize new instances of main controls: close/hide buttons 
            _closeButton = new CustomButton(_closeButtonIcon, 1, new Point(Width - 53, 5));
            HideButton = new CustomButton(_hideButtonIcon, 1, new Point(Width - 75, 3));

            // Customize theirs properties
            FormBorderStyle = FormBorderStyle.None;
            this.ResumeLayout(false);
            this.PerformLayout();

            // Assigning event handlers delagates
            this.MouseDown += new MouseEventHandler(FormMouseDown);
            this.MouseUp += new MouseEventHandler(FormMouseUp);
            this.MouseMove += new MouseEventHandler(FormMouseMove);
            this.MouseClick += new MouseEventHandler(ControlButtonMouseClick);
        }

        #endregion

        #region Event Handlers

        // On mouse down event handler
        private void FormMouseDown(object sender, MouseEventArgs e)
        {
            //On Mouse Down set the flag _drag to true and save the clicked point to the _startPoint variable
            this._drag = true;
            this._startPoint = new Point(e.X, e.Y);
        }

        // On mouse up event handler
        private void FormMouseUp(object sender, MouseEventArgs e)
        {
            //Set _drag flag to false;
            this._drag = false;
        }

        // On mouse move event handler
        private void FormMouseMove(object sender, MouseEventArgs e)
        {
            //If flag _drag is true, so drag the form
            if (this._drag)
            {
                Point p1 = new Point(e.X, e.Y);
                Point p2 = this.PointToScreen(p1);
                Point p3 = new Point(p2.X - this._startPoint.X,
                                     p2.Y - this._startPoint.Y);
                this.Location = p3;
            }
        }


        // Mouse click on custom buttons
        protected void ControlButtonMouseClick(object sender, MouseEventArgs e)
        {
            // If click point is inside of custom button handle the event
            if (_closeButton.Area.Contains(e.Location))
            {
                Close();
            }
            else if (HideButton.Area.Contains(e.Location))
            {
                MinimizeToTray();
            }
        }

        // Minimaze the window to taskbar
        private void MinimizeToTray()
        {
            // Check if currently minimazed so set state to normal
            if (FormWindowState.Minimized == WindowState)
            {
                WindowState = FormWindowState.Normal;
            }
            else if (FormWindowState.Normal == WindowState)
            {
                // ...or let minimaze the form 
                this.WindowState = FormWindowState.Minimized;
            }
        }

        #endregion

        #region Methods

        // Each control owned rednering logic
        internal static void RenderControl(PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            _closeButton.RenderControl(e.Graphics);
            HideButton.RenderControl(e.Graphics);
        }

        // Close the window
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Visible = false;
        }

        // Initialize entry point to system gdi and pass parameters of custom form
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        internal static extern IntPtr CreateRoundRectRgn
        (
        int nLeftRect,          // x-coordinate of upper-left corner
        int nTopRect,           // y-coordinate of upper-left corner
        int nRightRect,         // x-coordinate of lower-right corner
        int nBottomRect,        // y-coordinate of lower-right corner
        int nWidthEllipse,      // height of ellipse
        int nHeightEllipse      // width of ellipse
        );

        #endregion

    }
}
