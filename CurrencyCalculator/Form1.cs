﻿//////////////////////////////////////////////////////////////////////////////
///  This application supplies thesis work "The practical usage of unit    ///
/// testing on example of development web-client"                          ///
///                                                                        ///
/// Student: Alexander Vizner                                              ///
/// Place: Tallinn                                                         ///
/// Year: 2012                                                             ///
//////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;

namespace CurrencyCalculator
{

    /// <summary>
    /// Main application form
    /// </summary>
    public sealed partial class Form1 : CustomForm
    {

        #region Fields

        public Dictionary<string, Bitmap> IconsList = new Dictionary<string, Bitmap>();
        private Dictionary<string, double> _ratesList = new Dictionary<string, double>();
        private Dictionary<string, string> _currencyCodesList = new Dictionary<string, string>();
        private readonly Dictionary<string, string> _actualList = new Dictionary<string, string>();
        private readonly string _ratesXmlPath = Path.Combine(Environment.CurrentDirectory, @"..\..\Resources\eurofxref-daily.xml");
        private readonly string _codesXmlPath = Path.Combine(Environment.CurrentDirectory, @"..\..\Resources\Codes.xml");
        private static readonly Uri ecbUrl = new Uri("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
        private string _currentSelectedCurrency1;
        private string _currentSelectedCurrency2;
        private NetworkManager netManager;
        private int _timeout = 60;
        private bool _offline = false;

        #endregion

        #region Constructors

        public Form1()
        {
            InitializeComponent();
            netManager = new NetworkManager();
            bool tmp =
                Updater.IsValidXml(Path.Combine(Environment.CurrentDirectory, @"..\..\Resources\eurofxref-daily.xml"));

            // First time setup: load default currencies and their pictos
            _currentSelectedCurrency1 = "EUR";
            _currentSelectedCurrency2 = "AUD";
            this.pictureBox1.Image = new Bitmap(Path.Combine(Environment.CurrentDirectory, @"..\..\Resources\Flags\EUR.png"));
            this.pictureBox2.Image = new Bitmap(Path.Combine(Environment.CurrentDirectory, @"..\..\Resources\Flags\AUD.png"));

            if (Updater.IsValidXml(_ratesXmlPath) && Updater.IsValidXml(_codesXmlPath))
            {
                // If xml files are not broked try to load
                TryToLoadXml();
            }
            else
            {
                // Otherwise try to update ecb xml from internet
                TryToUpdate();
            }



            // Adding euro as start unit
            _ratesList.Add("EUR", 1.0);

            _actualList = Parser.Intersect(_ratesList, _currencyCodesList);

            BindingSource bs1 = new BindingSource { DataSource = new List<string>(_actualList.Values) };
            BindingSource bs2 = new BindingSource { DataSource = new List<string>(_actualList.Values) };
            combo1.DataSource = bs1;
            combo2.DataSource = bs2;
            label.Text = string.Format("Last update: {0}",
                Parser.GetTimeOfUpdate(
                Path.Combine(Environment.CurrentDirectory,
                @"..\..\Resources\eurofxref-daily.xml")).ToShortDateString()
                );

            // Default selection
            combo1.SelectedIndex = combo1.FindString("Euro");
            combo2.SelectedIndex = combo1.FindString("Australia dollar");
            combo2.SelectedIndex = 0;

            // Initial values 
            cTextBox1.Text = "1";
            cTextBox2.Text = "1";

            MouseClick += new MouseEventHandler(SettingsButtonMouseClick);

            if (!_offline)
                Updater.Initializer(_timeout);
        }

        #endregion

        #region Event Handlers

        private void ComboSelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;

            // Load corresponding pictogramm of flag by currency name
            LoadPictogramm(cb);

            if (cb.Name == "combo1")
            {
                cTextBox1.Text = Convert(Double.Parse(cTextBox1.Text),
                                         _currentSelectedCurrency1,
                                         _currencyCodesList.FirstOrDefault(x => x.Value == cb.SelectedItem.ToString()).
                                             Key);
                _currentSelectedCurrency1 =
                    _currencyCodesList.FirstOrDefault(n => n.Value == (string)cb.SelectedItem).Key;
            }
            else
            {
                cTextBox2.Text = Convert(Double.Parse(cTextBox2.Text),
                                         _currentSelectedCurrency2,
                                         _currencyCodesList.FirstOrDefault(x => x.Value == cb.SelectedItem.ToString()).
                                             Key);
                _currentSelectedCurrency2 =
                    _currencyCodesList.FirstOrDefault(n => n.Value == (string)cb.SelectedItem).Key;
            }
        }

        // Handle txtBox vale was changed
        private void TextBoxTextChanged(object sender, KeyEventArgs e)
        {
            TextBox txt = (TextBox)sender;

            // Check if input is digits from 0 to 9 OR numpads digit 
            // or delete/ return keys were pressed 
            if ((e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9) ||
                (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9) ||
                (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Return))
            {

                // Avoiding empty textbox using zero instead
                if (txt.Text != "")
                {
                    switch (txt.Name)
                    {
                        case "textBox1":
                            cTextBox2.Text = Convert(Double.Parse(cTextBox1.Text),
                                                     _currencyCodesList.FirstOrDefault(
                                                         n => n.Value == (string)combo1.SelectedItem).Key,
                                                     _currencyCodesList.FirstOrDefault(
                                                         n => n.Value == (string)combo2.SelectedItem).Key);
                            break;
                        case "textBox2":
                            cTextBox1.Text = Convert(Double.Parse(cTextBox2.Text),
                                                     _currencyCodesList.FirstOrDefault(
                                                         n => n.Value == (string)combo2.SelectedItem).Key,
                                                     _currencyCodesList.FirstOrDefault(
                                                         n => n.Value == (string)combo1.SelectedItem).Key);
                            break;
                    }
                }
            }
        }

        // Load flags pictogramm by currency code
        private void LoadPictogramm(ComboBox cb)
        {
            // Getting currency code by full name
            string name = _actualList.FirstOrDefault(x => x.Value == cb.SelectedItem.ToString()).Key;
            string path = string.Format("{0}\\{1}.png",
                                        Path.Combine(Environment.CurrentDirectory, @"..\..\Resources\Flags"), name);

            // Changing currency icon depends to selection
            foreach (Control control in Controls)
            {
                if (control.Name.Equals(string.Format("pictureBox{0}", cb.Name.Last())))
                {
                    PictureBox pb = control as PictureBox;
                    if (pb != null) pb.Image = File.Exists(path) ? new Bitmap(path) : new Bitmap(22, 15);
                }
            }

        }

        // Convert and rounding to 3 digits
        private string Convert(double amount, string from, string to)
        {
            double ret = 0;

            // If network connection is available converting using google service
            if (netManager.IsConnected())
            {
                ret = NetworkManager.RequestGoogle(amount, from, to);
            }
            else
            {
                // else convert using saved rates xml
                ret = amount * (_ratesList.FirstOrDefault(n => n.Key == to).Value);
            }
            return string.Format("{0}", Math.Round(ret, 3));
        }

        // Getting timout value from settings dialog if OK
        private void SettingsButtonMouseClick(object sender, MouseEventArgs e)
        {
            if (settingsButton.Area.Contains(e.Location))
            {
                using (Settings settingsDialog = new Settings())
                {
                    DialogResult result = settingsDialog.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        _timeout = settingsDialog.Timeout;
                        _offline = settingsDialog.OfflineMode;
                    }
                }
            }
        }

        #endregion

        #region Methods

        protected override void OnPaint(PaintEventArgs e)
        {
            settingsButton.RenderControl(e.Graphics);
            Region = Region.FromHrgn(CreateRoundRectRgn(0, 0, Width - 9, Height - 9, 9, 9));
            CustomForm.RenderControl(e);
        }

        private void TryToLoadXml()
        {
            // Try to load currencies rate and theirs full name
            try
            {
                _currencyCodesList = Parser.GetCodesMap(_codesXmlPath);
                _ratesList = Parser.GetRatesList(_ratesXmlPath);
            }
            catch (Exception e)
            {
                throw new FileNotFoundException();
            }
        }

        private void TryToUpdate()
        {
            XDocument ecbResponce = new XDocument();
            int counter = 3;

            do
            {
                ecbResponce = netManager.DoSingleRequest(ecbUrl);

                // Parse the response and store rates in dictionary
                Parser.GetRatesList(ecbResponce);

                // ...and store it locally
                netManager.SaveRequestToFile(ecbResponce, _ratesXmlPath);

                if (Updater.IsValidXml(_ratesXmlPath))
                {
                    break;
                }
                MessageBox.Show(string.Format("Unable to fetching rates from internet. Attempt: {0}", counter));
                counter--;
            } while (counter == 0);
        }

        #endregion

    }
}
