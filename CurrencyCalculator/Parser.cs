﻿//////////////////////////////////////////////////////////////////////////////
///  This application supplies thesis work "The practical usage of unit    ///
/// testing on example of development web-client"                          ///
///                                                                        ///
/// Student: Alexander Vizner                                              ///
/// Place: Tallinn                                                         ///
/// Year: 2012                                                             ///
//////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;

namespace CurrencyCalculator
{
    /// <summary>
    /// Utilities class deals with xml in following ways:
    ///   - getting code, rates from xml
    ///   - finding the same entries
    ///   - getting xml in whole
    /// </summary>
    class Parser
    {
        // Getting current culture
        private static readonly CultureInfo Culture = new CultureInfo("en-US");

        #region Methods

        /// <summary>
        /// Returns currency rates from storage file
        /// </summary>
        /// <param name="path">Path to xml file</param>
        /// <returns>Dictionary of string and double></returns>
        public static Dictionary<string, double> GetRatesList(string path)
        {
            XDocument doc = new XDocument();

            // Try to load the file byy given path or rise exception 
            try
            {
                doc = XDocument.Load(path);
            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format("{0} Unable to load xml file: {1}", DateTime.Now, e.Message));
            }
            return GetRatesList(doc);
        }

        /// <summary>
        /// Returns currency rates from xml
        /// </summary>
        /// <param name="doc">XML in memory</param>
        /// <returns>Dictionary of string and  double></returns>
        public static Dictionary<string, double> GetRatesList(XDocument doc)
        {
            XNamespace ns = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref";

            // Iterate in cubes and getting all currency values
            var cubes = doc.Descendants(ns + "Cube")
                           .Where(x => x.Attribute("currency") != null)
                           .Select(x => new
                           {
                               Currency = (string)x.Attribute("currency"),
                               Rate = (double)x.Attribute("rate")
                           });

            return cubes.ToDictionary(cube => cube.Currency, cube => Convert.ToDouble(cube.Rate));
        }

        /// <summary>
        /// Returns date of last update of document from file
        /// </summary>
        /// <returns>DateTime</returns>
        public static DateTime GetTimeOfUpdate(string path)
        {
            XDocument doc = XDocument.Load(path);
            return GetTimeOfUpdate(doc);
        }

        /// <summary>
        /// Returns date of last update from xml in memory
        /// </summary>
        /// <param name="doc">XML document</param>
        /// <returns>DateTime</returns>
        public static DateTime GetTimeOfUpdate(XDocument doc)
        {
            DateTime dt = new DateTime();
            XNamespace ns = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref";

            var time = from n in doc.Descendants(ns + "Cube")
                       where n.Attribute("time") != null
                       select n.Attribute("time").Value;

            return Convert.ToDateTime(time.FirstOrDefault(), Culture);
        }
        
        /// <summary>
        /// Returns currency map to theirs full name
        /// </summary>
        /// <param name="path">Full path to xml file</param>
        /// <returns>Dictionary of string and string</returns>
        public static Dictionary<string, string> GetCodesMap(string path)
        {
            XDocument doc = null;

            // Check if path string is not empty and really exists
            if (!string.IsNullOrEmpty(path) && File.Exists(path))
                try
                {
                    doc = XDocument.Load(path);
                }
                catch (Exception e)
                {
                    MessageBox.Show(string.Format("{0} Unable to load xml file: {1}", DateTime.Now, e.Message));
                }

            // Iterate and get all codes
            var codes = (from n in doc.Descendants("currency")
                         select new
                         {
                             name = n.Attribute("code").Value,
                             description = n.Value
                         }).ToDictionary(key => key.name, value => value.description);
            return codes;
        }

        /// <summary>
        /// Getting the same entries in two given dictionaries
        /// </summary>
        /// <param name="ratesList">First dictionary</param>
        /// <param name="codesList">Second dictinary</param>
        /// <returns></returns>
        public static Dictionary<string, string> Intersect(Dictionary<string, double> ratesList, Dictionary<string, string> codesList)
        {
            // Declare and initialize dictionary of results
            Dictionary<string, string> result = new Dictionary<string, string>() ;
            foreach (KeyValuePair<string, double> pair in ratesList)
            {
                string i;

                // If key found put it to results
                if(codesList.TryGetValue(pair.Key, out i))
                {
                    result.Add(pair.Key, i);
                }
            }
           
            return result;
        }

        /// <summary>
        /// Returns rate value by given currency code
        /// </summary>
        /// <param name="rateList">Dictionary</param>
        /// <param name="code">c</param>
        /// <returns></returns>
        public static double GetRateByCode(Dictionary<string, double> rateList, string code)
        {
            double ret = 0;
            // Values iterating of given dictionary and search for code
            foreach (KeyValuePair<string, double> pair in rateList.Where(pair => pair.Key == code))
            {
                ret = pair.Value;
            }

            // If not found return 0
            return ret;
        }
        #endregion
        
    }
}
