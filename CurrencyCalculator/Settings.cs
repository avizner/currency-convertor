﻿//////////////////////////////////////////////////////////////////////////////
///  This application supplies thesis work "The practical usage of unit    ///
/// testing on example of development web-client"                          ///
///                                                                        ///
/// Student: Alexander Vizner                                              ///
/// Place: Tallinn                                                         ///
/// Year: 2012                                                             ///
//////////////////////////////////////////////////////////////////////////////

using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace CurrencyCalculator
{
    class Settings : CustomForm
    {
        #region Fields

        private readonly Label _label1;
        private static Label _label2;
        private static CustomTextBox _textBox;
        private readonly CheckBox _checkBox;
        private readonly CustomButton _applyButton;
        private readonly Bitmap _applyImage;

        public int Timeout { get; set; }
        public bool OfflineMode { get; set; }

        #endregion

        #region Constractors

        public Settings()
        {
            try
            {
                _applyImage = new Bitmap(Path.Combine(Environment.CurrentDirectory, @"..\..\Resources\Icons\Apply.png"));
            }
            catch (FileNotFoundException e)
            {
                MessageBox.Show("During loading application error occures: {0}", e.Message);
            }

            _applyButton = new CustomButton(_applyImage, 1, new Point(Width - 53, 94));

            _label1 = new Label();
            _label1.Parent = this;
            _label2 = new Label();
            _label2.Parent = this;
            _textBox = new CustomTextBox();
            _textBox.Parent = this;
            _textBox.Text = "30";
            _checkBox = new CheckBox();
            _checkBox.Parent = this;
            _checkBox.Checked = true;
            this.MouseClick += new MouseEventHandler(ControlButtonMouseClick);
            _checkBox.CheckedChanged += new EventHandler(CheckBoxCheckedChanged);
        }

        private void CheckBoxCheckedChanged(object sender, EventArgs e)
        {
            CheckBox chb = (CheckBox)sender;
            OfflineMode = _textBox.Enabled = chb.Checked;
            _label2.ForeColor = (OfflineMode) ? Color.WhiteSmoke : Color.Gray;
        }

        #endregion

        #region Event handlers

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Region = Region.FromHrgn(CreateRoundRectRgn(0, 0, Width - 9, Height - 9, 9, 9));
            RenderControl(e);
            BackColor = Color.Black;
            Opacity = 0.83;
            ClientSize = new Size(284, 130);

            _label1.Size = new Size(150, 21);
            _label1.Location = new Point(25, 30);
            _label1.Text = "Enable online rates update:";
            _label1.ForeColor = Color.WhiteSmoke;
            _label1.Font = new Font("Tahoma", 9.0F);
            _checkBox.Location = new Point(180, 26);

            _label2.Size = new Size(135, 21);
            _label2.Location = new Point(25, 60);
            _label2.Text = "Update every minutes:";
            _label2.ForeColor = Color.WhiteSmoke;
            _label2.Font = new Font("Tahoma", 9.0F);
            _textBox.Size = new Size(30, 21);
            _textBox.Location = new Point(160, 57);

            _applyButton.RenderControl(e.Graphics);

        }

        // Apply button press event handler
        protected new void ControlButtonMouseClick(object sender, MouseEventArgs e)
        {
            if (_applyButton.Area.Contains(e.Location))
            {
                DialogResult = DialogResult.OK;
                Timeout = Convert.ToInt32(_textBox.Text);
                Close();
            }
        }

        #endregion

        #region Methods

        #endregion




    }
}
