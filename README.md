For running unit tests for CurrencyCalculator NUnit framework should be installed first.
Notice that after installation in Visual Studio will be available runner from both, context menu and additional pane "Unit Test Session". 

Step by step installation and running: 
1. Please find binaries and source in NUnit framework directory.
2. Select way you desire, but for smooth installation, *.msi installation is highly recommended
3. After successful installation open the solution project file located in 0.1 directory
4. Compile the application 
5. Open tests class in VS 
6. Find Run unit tests item in context menu and select it
7. The result should be shown in "Unit Test Session" pane on the bottom of the VS

If some troubles are occurred please refer to documentation in "NUnit framework" folder or
directly to Quick start wiki on NUnit official site (http://www.nunit.org/index.php?p=quickStart&r=2.2.10)

If happened some unexpected, please contact me via e-mail: alexandr.vizner@gmail.com